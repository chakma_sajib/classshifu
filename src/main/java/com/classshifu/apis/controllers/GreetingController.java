package com.classshifu.apis.controllers;

import com.classshifu.apis.models.Greeting;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import java.util.concurrent.atomic.AtomicLong;


@RestController
public class GreetingController {

    private static final String template = "Hello, %s";
    private final AtomicLong atomicLong = new AtomicLong();
    @GetMapping("/greeting")
    public Greeting greeting(@RequestParam(value = "value",  defaultValue = "World") String name){
        return new Greeting(atomicLong.incrementAndGet(),String.format(template, name));
    }
}
