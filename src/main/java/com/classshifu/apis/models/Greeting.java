package com.classshifu.apis.models;

public class Greeting {
    private final long Id;
    private final String content;

    public Greeting(long Id, String content){
        this.Id = Id;
        this.content = content;
    }

    public long getId() {
        return Id;
    }

    public String getContent() {
        return content;
    }
}
